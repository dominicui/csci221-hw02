/*
 * Person.h
 *
 *  Created on: 2017��10��26��
 *      Author: Domin
 */
#include <string>;

#ifndef PERSON_H_
#define PERSON_H_

class Person {
public:
	Person();
	virtual ~Person();
	std::string getId();
	std::string getName();
private:
	std::string id;
	std::string name;

};

#endif /* PERSON_H_ */
