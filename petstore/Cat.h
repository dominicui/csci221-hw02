/*
 * Cat.h
 *
 *  Created on: 2017��10��26��
 *      Author: Domin
 */
#include <string>;

#ifndef CAT_H_
#define CAT_H_

class Cat {
public:
	Cat();
	virtual ~Cat();
	std::string getCoat();
	bool setCoat(std::string length);
private:
	std::string coat;

};

#endif /* CAT_H_ */
