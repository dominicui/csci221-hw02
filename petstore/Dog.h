/*
 * Dog.h
 *
 *  Created on: 2017��10��26��
 *      Author: Domin
 */
#include <string>;

#ifndef DOG_H_
#define DOG_H_

class Dog {
public:
	Dog();
	virtual ~Dog();
	std::string getBreed();
private:
	std::string breed;

};

#endif /* DOG_H_ */
