/*
 * Animal.h
 *
 *  Created on: 2017��10��26��
 *      Author: Domin
 */
#include <string>;

#ifndef ANIMAL_H_
#define ANIMAL_H_

class Animal {
public:
	Animal();
	virtual ~Animal();
	std::string getBreed();
	std::string getCoat();
	bool setCoat(std::string length);
	double getPrice();
	bool setPrice(double amount);
	std::string getName();
	bool setName(std::string name);
private:
	std::string name;
	double price;
};

#endif /* ANIMAL_H_ */
