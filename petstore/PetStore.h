/*
 * PetStore.h
 *
 *  Created on: 2017��10��26��
 *      Author: Domin
 */
#include "Animal.h"
#include "Person.h"

#ifndef PETSTORE_H_
#define PETSTORE_H_

class PetStore {
public:
	PetStore();
	virtual ~PetStore();
	std::string getName();
private:
	std::string name;
	Person* customers;
	Animal* items;

};

#endif /* PETSTORE_H_ */
